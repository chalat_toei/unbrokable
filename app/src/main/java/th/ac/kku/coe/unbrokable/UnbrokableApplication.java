package th.ac.kku.coe.unbrokable;

import android.app.Application;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import th.ac.kku.coe.unbrokable.manager.DataManager;

/**
 * Created by crazynova on 3/5/2559.
 */
public class UnbrokableApplication extends Application {

    DataManager dm;

    @Override
    public void onCreate() {
        super.onCreate();

        //Initilize things here
        Contextor.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
