package th.ac.kku.coe.unbrokable.adapter;

import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.fragment.BudgetFragment;
import th.ac.kku.coe.unbrokable.manager.DataManager;

/**
 * Created by crazynova on 4/5/2559.
 */
public class BudgetAdapter extends BaseAdapter {

    DataManager dm;
    Context context;
    BudgetFragment budgetFragment;

    ImageView budgetImage;
    TextView tvBudgetType, tvBudgetAmount;
    Button btnAdd, btnSpend;

    ArrayList<Integer> imgId, amount;
    ArrayList<String> budgetType;

    public BudgetAdapter(Context context, ArrayList imgId, ArrayList amount, ArrayList budgetType, BudgetFragment budgetFragment) {
        this.context = context;
        this.imgId = imgId;
        this.amount = amount;
        this.budgetType = budgetType;
        this.budgetFragment = budgetFragment;
    }

    public void setBudgetItemAdapter(ArrayList imgId, ArrayList amount, ArrayList budgetType) {
        this.imgId = imgId;
        this.amount = amount;
        this.budgetType = budgetType;
    }

    @Override
    public int getCount() {
        return imgId.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater mInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = mInflater.inflate(R.layout.custom_view_group_budget, parent, false);

        dm = DataManager.getInstance();

        budgetImage = (ImageView) view.findViewById(R.id.img_view_budget);
        tvBudgetType = (TextView) view.findViewById(R.id.tv_budget_type);
        tvBudgetAmount = (TextView) view.findViewById(R.id.tv_budget_amount);
        btnAdd = (Button) view.findViewById(R.id.btn_budget_add);
        btnSpend = (Button) view.findViewById(R.id.btn_budget_spend);


        budgetImage.setImageResource(getIcon(imgId.get(position)));
        tvBudgetType.setText(budgetType.get(position));
        tvBudgetAmount.setText(amount.get(position) + " ฿");

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(context)
                        .title(R.string.txt_dialog_add_saving_title)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(context.getString(R.string.txt_dialog_add_saving_hint), null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                try {
                                    //Log.i("Dialog", input.toString());
                                    int amount = Integer.parseInt(input.toString());
                                    dm.addBudgetBalance(position, amount);
                                    budgetFragment.updateBudgetView();
                                } catch (NumberFormatException e) {
                                    Toast.makeText(context, R.string.txt_dialog_add_saving_toast, Toast.LENGTH_SHORT).show();
                                }
                                }
                            }

                            ).

                            show();

                            //Log.i("checkclick", "click add : " + position);
                        }
            });

        btnSpend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(context)
                        .title(R.string.txt_dialog_spening_title)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(context.getString(R.string.txt_dialog_spening_hint), null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                try {
                                    //Log.i("Dialog", input.toString());
                                    int amount = Integer.parseInt(input.toString());
                                    dm.removeBudgetBalance(context, position, amount);
                                    budgetFragment.updateBudgetView();
                                } catch (NumberFormatException e) {
                                    Toast.makeText(context, R.string.txt_dialog_spending_toast, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).show();
                //Log.i("checkclick", "click add : " + position);
            }
        });

        return view;
    }


    private int getIcon(int icon_num) {
        int imgID = 0;
        if(icon_num == 1) imgID = R.drawable.icon_book ;
        if(icon_num == 2)  imgID = R.drawable.icon_bus;
        if(icon_num == 3)    imgID = R.drawable.icon_education;
        if(icon_num == 4) imgID = R.drawable.icon_food;
        if(icon_num == 5) imgID = R.drawable.icon_medicare;
        if(icon_num == 6)    imgID = R.drawable.icon_movie;
        if(icon_num == 7)  imgID = R.drawable.icon_pet;
        if(icon_num == 8) imgID = R.drawable.icon_shop;
        if(icon_num == 9)    imgID = R.drawable.icon_sport;
        if(icon_num == 10)   imgID = R.drawable.icon_travel;
        return imgID;
    }

}
