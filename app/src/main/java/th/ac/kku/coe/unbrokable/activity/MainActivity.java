package th.ac.kku.coe.unbrokable.activity;


import android.content.Intent;
import android.content.res.Configuration;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;


import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.adapter.ViewPagerAdapter;
import th.ac.kku.coe.unbrokable.fragment.BudgetFragment;
import th.ac.kku.coe.unbrokable.fragment.DailyBudgetFragment;
import th.ac.kku.coe.unbrokable.fragment.GoalFragment;
import th.ac.kku.coe.unbrokable.manager.DataManager;

public class MainActivity extends AppCompatActivity {

    //test update code in bitbucket

    DataManager dm;

    final static String TAG = "mainactivity";
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    FloatingActionButton fabBtn;
    LinearLayout rootLayout;
    android.support.v7.widget.Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    NavigationView navigation;
    MenuItem actionAdd;
    String txt_frag_daily_budget, txt_frag_goal, txt_frag_total_budget;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initInstances();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if(intent != null) {
            int lastFragment = intent.getIntExtra("selectFragment", -99);
            if(lastFragment != 99)
            viewPager.setCurrentItem(lastFragment);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("checkonfunc", "mainActivity on resume");

        dm = DataManager.getInstance();
        dm.getCurrentAmount(dm.getUsername());

        int currentTab = viewPager.getCurrentItem();
        if(currentTab == 0) {
            fabBtn.show();
        } else if (currentTab == 1) {
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("checkonfunc", "mainActivity on pause");
    }

    private void initInstances() {

        rootLayout = (LinearLayout) findViewById(R.id.rootLayout);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigation = (NavigationView) findViewById(R.id.navigation);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        fabBtn = (FloatingActionButton) findViewById(R.id.fabBtn);
        actionAdd = (MenuItem) findViewById(R.id.action_add);

        txt_frag_daily_budget = getResources().getString(R.string.txt_frag_daily_budget);
        txt_frag_goal= getResources().getString(R.string.txt_frag_goal);
        txt_frag_total_budget = getResources().getString(R.string.txt_frag_monthly_budget);

        setSupportActionBar(toolbar);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new DailyBudgetFragment(), txt_frag_daily_budget);
        viewPagerAdapter.addFragments(new BudgetFragment(), txt_frag_total_budget);
        viewPagerAdapter.addFragments(new GoalFragment(), txt_frag_goal);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        fabBtn.hide();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        navigation.setItemIconTintList(null);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.profile_id:
                        Intent intent = new Intent(MainActivity.this, ShowProfileActivity.class);
                        //Log.i("Transition", "startLoadProfileActivity");
                        startActivity(intent);
                        break;
                    case R.id.daily_budget_id:
                        viewPager.setCurrentItem(0);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.total_budget_id:
                        viewPager.setCurrentItem(1);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.goal_id:
                        viewPager.setCurrentItem(2);
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.statistics_daily_id:
                        intent = new Intent(MainActivity.this, StatisticsActivity.class);
                        //Log.i("Transition", "startLoadStaticActivity");
                        startActivity(intent);
                        break;
                    case R.id.statistics_monthly_id:
                        intent = new Intent(MainActivity.this, MonthlyStatisticsActivity.class);
                        startActivity(intent);
                        break;
                }
                return false;
            }
        });


        fabBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go to Add
                //Log.i("Transition", "start AddDailyBudgetActivity");
                Intent intent = new Intent(MainActivity.this, AddDailyBudgetActivity.class);
                startActivity(intent);
            }
        });
        viewPager.setOnPageChangeListener(mPageChangeListener);
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        if (id == R.id.action_add) {
            int currentFragment = viewPager.getCurrentItem();
            if(currentFragment == 0) {
                //Log.i("Transition", "start AddDailyBudgetActivity");
                Intent intent = new Intent(this, AddDailyBudgetActivity.class);
                startActivity(intent);
            } else if(currentFragment == 1){
                //Log.i("Transition", "start AddBudgetActivity");
                Intent intent = new Intent(this, AddBudgetActivity.class);
                startActivity(intent);
            } else if(currentFragment == 2) {
                //Log.i("Transition", "start AddGoalActivity");
                Intent intent = new Intent(this, AddGoalActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(MainActivity.this,"Don't work", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if(position == 0) {
                fabBtn.show();
            } else if(position == 1) {
                try {
                    BudgetFragment budgetFragment = (BudgetFragment) getSupportFragmentManager().getFragments().get(0);
                    budgetFragment.updateBudgetView();
                } catch (NullPointerException|ClassCastException e) {

                }
                fabBtn.hide();
            } else {
                fabBtn.hide();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };




}
