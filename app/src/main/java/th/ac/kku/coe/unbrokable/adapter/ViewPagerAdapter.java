package th.ac.kku.coe.unbrokable.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by lenovo1 on 6/4/2559.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment>fragments = new ArrayList<>();
    ArrayList<String>tabTitles = new ArrayList<>();

    public void addFragments(Fragment fragment,String titles){
        this.fragments.add(fragment);
        this.tabTitles.add(titles);
    }

    public ViewPagerAdapter(android.support.v4.app.FragmentManager fm)
    {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public CharSequence getPageTitle(int position){
        return  tabTitles.get(position);
    }

}
