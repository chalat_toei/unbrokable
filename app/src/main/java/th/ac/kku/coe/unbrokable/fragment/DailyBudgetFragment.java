package th.ac.kku.coe.unbrokable.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.adapter.DailyBudgetAdapter;
import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.manager.DataManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class DailyBudgetFragment extends Fragment {

    private View view;
    FrameLayout noListDailyBG;
    TextView tvCurrentAmount,tvAmountPerDay;
    DailyBudgetAdapter adapter;
    ListView dailyBudgetItemList;
    DataManager dm;
    String username;
    static DailyBudgetFragment dailyBudgetFragment;
    ArrayList<Integer> amountList = new ArrayList<Integer>();
    ArrayList<String> noteList = new ArrayList<String>();
    ArrayList<String> amountTypeList = new ArrayList<String>();


    public DailyBudgetFragment() {
        super();
        dm = DataManager.getInstance();
        username = dm.getUsername();
    }

    public static DailyBudgetFragment newInstance() {
        dailyBudgetFragment = new DailyBudgetFragment();
        return dailyBudgetFragment;
    }

    public static DailyBudgetFragment getDailyBudgetFragment() {
        return dailyBudgetFragment;
    }


    @Override
    public void onStart() {
        super.onStart();
        //Log.i("checkOnFuncDailyBG", "On Start DailyBudget Fragment");
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.i("checkOnFuncDailyBG", "On Resume DailyBudget Fragment");
        updateDailyBudgetView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_daily_budget, container, false);
        //Log.i("checkOnFunc", "On create Yes!!!");
        dailyBudgetItemList = (ListView) view.findViewById(R.id.list_daily_budget);
        tvCurrentAmount = (TextView) view.findViewById(R.id.tv_current_amount);
        tvAmountPerDay = (TextView) view.findViewById(R.id.tv_amount_per_day);
        noListDailyBG = (FrameLayout) view.findViewById(R.id.bg_daily_budget_begin);

        tvAmountPerDay.setText(dm.amountperday + "");

        adapter = new DailyBudgetAdapter(view.getContext(), dm.getAmountList(), dm.getNoteList(), dm.getTypeList(), DailyBudgetFragment.this);
        dailyBudgetItemList.setAdapter(adapter);

        if(adapter.getCount() != 0) {
            noListDailyBG.setVisibility(View.GONE);
        }

        return view;
    }

    private void updateListView() {
        adapter.setDaliyBudgetItemAdapter(dm.getAmountList(), dm.getNoteList(), dm.getTypeList());
        adapter.notifyDataSetChanged();
        if(adapter.getCount() != 0) {
            noListDailyBG.setVisibility(View.GONE);
        }
    }

    public void updateDailyBudgetView() {
        //Log.i("Transition", "Update Daily Budget Views");
        dm.loadDailyBudgetData();
        tvCurrentAmount.setText(dm.getCurrentAmount(dm.getUsername()) + " ฿");
        updateListView();
    }


}
