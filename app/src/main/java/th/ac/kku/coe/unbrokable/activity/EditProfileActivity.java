package th.ac.kku.coe.unbrokable.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.manager.DataManager;

public class EditProfileActivity extends AppCompatActivity {

    DataManager dm;

    EditText etUsername,etBudgetPerDay,etBudgetPerMonth,etBudget;
    Button btnAddProfileDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initInstance();
    }

    private void initInstance() {
        dm = DataManager.getInstance();

        etUsername = (EditText) findViewById(R.id.etUsername);
        etBudgetPerDay = (EditText) findViewById(R.id.etBudgetPerDay);
        etBudgetPerMonth = (EditText) findViewById(R.id.etBudgetPerMonth);
        etBudget = (EditText) findViewById(R.id.etBudget);
        btnAddProfileDone = (Button) findViewById(R.id.btnAddProfileDone);

        /*
        String username = dm.getUsername();
        etUsername.setText(username);
        etBudgetPerDay.setText(dm.getAmountperday(username) + "");
        etBudgetPerMonth.setText(dm.getBudgetPerMonth(username) + "");
        etBudget.setText(dm.getCurrentBudget() + "");
        */

        btnAddProfileDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int budgetperday;
                if(etUsername.getText().toString().equals("")) {
                    Toast.makeText(EditProfileActivity.this, R.string.toast_edit_profile_name, Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        budgetperday = Integer.parseInt(etBudgetPerDay.getText().toString());
                        try {
                            dm.updateUserProfileData(etUsername.getText().toString(), budgetperday, Integer.parseInt(etBudgetPerMonth.getText().toString()));
                            dm.setCurrentBudget(Integer.parseInt(etBudget.getText().toString()));
                            Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                            startActivity(intent);
                        } catch (NumberFormatException e) {
                            Toast.makeText(EditProfileActivity.this, R.string.toast_edit_profile_total_budget, Toast.LENGTH_SHORT).show();
                        }
                    } catch (NumberFormatException e) {
                        Toast.makeText(EditProfileActivity.this, R.string.toast_edit_profile_BGPerDay, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
