package th.ac.kku.coe.unbrokable.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Bua on 5/3/2016.
 */
public class DBHelper {
    private static String TAG_NAME = "UnbrokableDB";
    private static final String DB_NAME = "UnbrokableApp.db";
    private static final int DB_VERSION = 2;
    //5 tables
    public static final String TABLE_USER = "UserProfile";
    public static final String TABLE_DAILYBG = "DailyBudget";
    public static final String TABLE_GOAL = "Goal";
    public static final String TABLE_BG = "Budget";
    public static final String TABLE_CURRENT_BALANCE = "CurrentBalance";
    public static final String TABLE_CURRENT_BUDGET = "CurrentBudget";

    //user profile table's attributes
    public static final String COL_USERNAME = "username";
    public static final String COL_AMOUNTPERDAY = "amountperday";
    public static final String COL_BUDGET = "budget";
    public static final String COL_IS_SELECT = "is_select";

    //Daily Budget table's attributes
    public static final String COL_DAILY_LIST_ID = "daily_list_id";
    public static final String COL_AMOUNT = "daily_amount";
    public static final String COL_AMOUNT_TYPE = "amount_type";
    public static final String COL_DATE = "date";
    public static final String COL_TIME = "time";
    public static final String COL_NOTE = "note";

    //Goal table's attributes
    public static final String COL_GOAL_ID = "goal_id";
    public static final String COL_GOAL_NAME = "goal_name";
    public static final String COL_GOAL_PRICE = "goal_price";
    public static final String COL_GOAL_MSAVE = "goal_msave"; //money save
    public static final String COL_GOAL_IMG_URI = "goal_img_uri";

    //budget table's attributes
    public static final String COL_BG_ID = "budget_id";
    public static final String COL_BG_TYPE = "budget_type";
    public static final String COL_BG_BALANCE = "budget_balance";
    public static final String COL_BG_ICON_ID = "budget_icon_id";

    //Current Balance's attribute
    //COL_DATE = "date";
    public static final String COL_CURRENT_BALANCE = "balance";


    //Current Budget's attribute
    public static final String COL_MONTH = "month";
    public static final String COL_CURRENT_BUDGET = "budget";

    private SQLiteDatabase db;
    private Context context;
    ArrayList<Integer> amountIdList, amountList;
    ArrayList<Integer> goalIdList, goalPriceList, goalMSaveList, goalImgIdList;
    ArrayList<Integer> budgetIdList, budgetBalanceList, budgetImgIdList;
    ArrayList<Integer> userDataInfo;

    String  username;
    ArrayList<String> dateList, timeList, noteList, typeList;
    ArrayList<String> goalNameList;
    ArrayList<String> budgetTypeList;




    public class OpenHelper extends SQLiteOpenHelper {

        public OpenHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            //crate table UserDoc
            db.execSQL("CREATE TABLE " + TABLE_USER +
                    "(" + COL_USERNAME + " TEXT PRIMARY KEY, "
                    + COL_AMOUNTPERDAY + " INTEGER, "
                    + COL_BUDGET + " INTEGER, "
                    + COL_IS_SELECT + " INTEGER);");
            //Log.i(TAG_NAME, "Create table " + TABLE_USER);


            //create table DailyBudget
            db.execSQL("CREATE TABLE " + TABLE_DAILYBG +
                    "(" + COL_DAILY_LIST_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COL_AMOUNT + " INTEGER, "
                    + COL_AMOUNT_TYPE + " TEXT, "
                    + COL_DATE + " TEXT, "
                    + COL_TIME + " TEXT, "
                    + COL_NOTE + " TEXT);");

            //Log.i(TAG_NAME, "Create table " + TABLE_DAILYBG);

            //create table Goal
            db.execSQL("CREATE TABLE " + TABLE_GOAL +
                    "("+ COL_GOAL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COL_GOAL_NAME + " TEXT, "
                    + COL_GOAL_PRICE + " INTEGER, "
                    + COL_GOAL_MSAVE + " INTEGER, "
                    + COL_GOAL_IMG_URI  + " TEXT);");

            //Log.i(TAG_NAME, "Create table " + TABLE_GOAL);

            //create table Budget
            db.execSQL("CREATE TABLE " + TABLE_BG +
                    "("+ COL_BG_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COL_BG_TYPE + " TEXT, "
                    + COL_BG_BALANCE + " INTEGER, "
                    + COL_BG_ICON_ID + " INTEGER);");

            //Log.i(TAG_NAME, "Create table " + TABLE_BG);

            //create table current balance
            db.execSQL("CREATE TABLE " + TABLE_CURRENT_BALANCE +
                    "("+ COL_DATE +" INTEGER PRIMARY KEY, "
                    + COL_CURRENT_BALANCE + " INTEGER);");

            //Log.i(TAG_NAME, "Create table " + TABLE_CURRENT_BALANCE);


            //create table current budget
            db.execSQL("CREATE TABLE " + TABLE_CURRENT_BUDGET +
                    "("+ COL_MONTH +" INTEGER PRIMARY KEY, "
                    + COL_CURRENT_BUDGET + " INTEGER);");

            //Log.i(TAG_NAME, "Create table " + TABLE_CURRENT_BUDGET);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //Log.i(TAG_NAME, "Upgrading database");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_DAILYBG + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_GOAL + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_BG + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CURRENT_BALANCE + ";");

            onCreate(db);
        }
    }

    public DBHelper(Context context){
        this.context = context;
        OpenHelper dbHelper = new OpenHelper(context);
        this.db = dbHelper.getWritableDatabase();
    }

    public String getDBUsername() {

        String username = "";
        Cursor cursor = this.db.query(TABLE_USER, null, null, null, null, null, null);

        if (cursor.moveToFirst()){
            do {
                username = cursor.getString(0);
                //Log.i("Loadformtableuser", "Load ok!");
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return username;
    }

    public ArrayList getamountList() {
        return amountList;
    }

    public ArrayList getamountIdList() {
        return amountIdList;
    }

    public ArrayList gettypeList(){
        return typeList;
    }

    public ArrayList getdateList(){
        return dateList;
    }

    public ArrayList gettimeList(){
        return timeList;
    }

    public ArrayList getnoteList(){
        return noteList;
    }

    public void scanTableDailyBudget(String date) {
        String whereClause = COL_DATE + " = ?";
        String[] whereArgs = new String[]{date};
        Cursor cursor = this.db.query(TABLE_DAILYBG, new String[]{COL_DAILY_LIST_ID,
                COL_AMOUNT, COL_AMOUNT_TYPE, COL_DATE, COL_TIME, COL_NOTE},
                whereClause, whereArgs, null, null, null);
        amountIdList = new ArrayList<>();
        amountList = new ArrayList<>();
        typeList = new ArrayList<>();
        dateList = new ArrayList<>();
        timeList = new ArrayList<>();
        noteList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                amountIdList.add(cursor.getInt(0));
                amountList.add(cursor.getInt(1));
                typeList.add(cursor.getString(2));
                dateList.add(cursor.getString(3));
                timeList.add(cursor.getString(4));
                noteList.add(cursor.getString(5));

                //Log.v("DBHelper", new Integer(cursor.getInt(0)).toString());
                //Log.v("DBHelper", new Integer(cursor.getInt(1)).toString());
                //Log.v("DBHelper", cursor.getString(2));
                //Log.v("DBHelper", cursor.getString(3));
                //Log.v("DBHelper", cursor.getString(4));
                //Log.v("DBHelper", cursor.getString(5));
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public int selectAmountByDate (int date) {
        String whereClause = COL_DATE + " = ?";
        String[] whereArgs = new String[]{Integer.toString(date)};
        Cursor cursor = this.db.query(TABLE_CURRENT_BALANCE, new String[]{COL_DATE, COL_CURRENT_BALANCE},
                whereClause,whereArgs,null,null,null);

        //default for can't get current balance
        int balance = -9999;
        if (cursor.moveToFirst()){
            do {
                balance = cursor.getInt(1);
                //Log.i("LDTBcurrentBalance", "Load balance = " + balance);
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return balance;
    }

    public int selectBudgetByMonth(int month) {
        //default for can't get current budget
        int budget = -9999;
        String whereClause = COL_MONTH + " = ?";
        String[] whereArgs = new String[]{Integer.toString(month)};
        Cursor cursor = this.db.query(TABLE_CURRENT_BUDGET, new String[]{COL_MONTH, COL_CURRENT_BUDGET},
                whereClause,whereArgs,null,null,null);

        if (cursor.moveToFirst()){
            do {
                budget = cursor.getInt(1);
                //Log.i("LDTBcurrentBudget", "Load budget = " + budget);
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return budget;
    }

    public ArrayList selectByUsername (String username) {
        String whereClause = "username = ?";
        String[] whereArgs = new String[]{username};
        Cursor cursor = this.db.query(TABLE_USER, new String[]{COL_USERNAME, COL_AMOUNTPERDAY, COL_BUDGET},
                whereClause,whereArgs,null,null,null);

        //Cursor cursor = this.db.rawQuery("SELECT * FROM " + TABLE_USER + " WHERE " + COL_USERNAME + " = " + username + ";", null);
        userDataInfo = new ArrayList<>();
        if (cursor.moveToFirst()){
            do {
                userDataInfo.add(cursor.getInt(1));
                userDataInfo.add(cursor.getInt(2));
                //Log.i("Loadformtableuser", "Load amount per day = " + cursor.getInt(1));
                //Log.i("Loadformtableuser", "Load budget = " + cursor.getInt(2));
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return userDataInfo;
    }

    //SELECT attribute from table GOAL
    public ArrayList selectGoalID (){
        ArrayList<Integer> goalIDList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_GOAL, new String[]{COL_GOAL_ID}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                goalIDList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return goalIDList;
    }

    public ArrayList selectGoalName (){
        ArrayList<String> goalNameList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_GOAL, new String[]{COL_GOAL_NAME},null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                goalNameList.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return goalNameList;
    }

    public ArrayList selectGoalPrice (){
        ArrayList<Integer> goalPriceList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_GOAL, new String[]{COL_GOAL_PRICE}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                goalPriceList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return goalPriceList;
    }

    public ArrayList selectGoalMSaved (){
        ArrayList<Integer> goalMSavedList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_GOAL, new String[]{COL_GOAL_MSAVE}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                goalMSavedList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return goalMSavedList;
    }

    public ArrayList selectGoalImgURI (){
        ArrayList<String> goalImgURIList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_GOAL, new String[]{COL_GOAL_IMG_URI}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                goalImgURIList.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return goalImgURIList;
    }

    public int selectMSavedByID (int goal_id){
        String whereClause = COL_GOAL_ID + " = " + goal_id;
        Cursor cursor = this.db.query(TABLE_GOAL, new String[]{COL_GOAL_ID, COL_GOAL_MSAVE},
                whereClause,null,null,null,null);

        //default for can't get current balance
        int mSaved = -9999;
        if (cursor.moveToFirst()){
            do {
                mSaved = cursor.getInt(1);
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return mSaved;
    }

    public ArrayList selectBGId (){
        ArrayList<Integer> budgetIdList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_BG, new String[]{COL_BG_ID}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                budgetIdList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return budgetIdList;
    }

    public ArrayList selectBGType (){
        ArrayList<String> budgetTypeList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_BG, new String[]{COL_BG_TYPE},null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                budgetTypeList.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return budgetTypeList;
    }

    public ArrayList selectBGBalance (){
        ArrayList<Integer> budgetBalanceList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_BG, new String[]{COL_BG_BALANCE}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                budgetBalanceList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return budgetBalanceList;
    }

    public ArrayList selectBGIconID (){
        ArrayList<Integer> budgetIconIDList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_BG, new String[]{COL_BG_ICON_ID}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                budgetIconIDList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return budgetIconIDList;
    }

    public int selectBalanceByID (int bg_id){
        String whereClause = COL_BG_ID + " = " + bg_id;
        Cursor cursor = this.db.query(TABLE_BG, new String[]{COL_BG_ID, COL_BG_BALANCE},
                whereClause,null,null,null,null);

        //default for can't get current balance
        int budgetBalance = -9999;
        if (cursor.moveToFirst()){
            do {
                budgetBalance = cursor.getInt(1);
            }while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return budgetBalance;
    }

    public ArrayList<Integer> selectDateList () {
        ArrayList<Integer> dateList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_CURRENT_BALANCE, new String[]{COL_DATE}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                dateList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return dateList;
    }

    public ArrayList<Integer> selectMonthList() {
        ArrayList<Integer> monthList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_CURRENT_BUDGET, new String[]{COL_MONTH},null,null,null,null,null);
        if(cursor.moveToFirst()){
            do {
            monthList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
                cursor.close();
        }
        return monthList;
    }

    public ArrayList selectCurrentAmountList() {
        ArrayList<Integer> currentAmountList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_CURRENT_BALANCE, new String[]{COL_CURRENT_BALANCE}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                currentAmountList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return currentAmountList;
    }

    public ArrayList selectMonthlyBudgetList() {
        ArrayList<Integer> budgetList = new ArrayList<>();
        Cursor cursor = this.db.query(TABLE_CURRENT_BUDGET, new String[]{COL_CURRENT_BUDGET}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do {
                budgetList.add(cursor.getInt(0));
            }while (cursor.moveToNext());
        }
        if(cursor != null && !cursor.isClosed()){
            cursor.close();
        }
        return budgetList;
    }

    public void insertUserProfile(String username, int amountperday, int budget){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_USERNAME, username);
        contentValues.put(COL_AMOUNTPERDAY, amountperday);
        contentValues.put(COL_BUDGET, budget);
        contentValues.put(COL_IS_SELECT, 1);
        db.insert(TABLE_USER, null, contentValues);
    }

    public void insertDailyBGItem(int amount, String amount_type, String date, String time, String note){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_AMOUNT, amount);
        contentValues.put(COL_AMOUNT_TYPE, amount_type);
        contentValues.put(COL_DATE, date);
        contentValues.put(COL_TIME, time);
        contentValues.put(COL_NOTE, note);
        db.insert(TABLE_DAILYBG, null, contentValues);
    }

    public void insertGoalItem(String goalName, int goalPrice, int goalSaved, String goalImageURI){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_GOAL_NAME, goalName);
        contentValues.put(COL_GOAL_PRICE, goalPrice);
        contentValues.put(COL_GOAL_MSAVE, goalSaved);
        contentValues.put(COL_GOAL_IMG_URI, goalImageURI);
        db.insert(TABLE_GOAL, null, contentValues);
    }

    public void insertBudgetItem(String bg_type, int bg_balance, int bg_icon_id ){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BG_TYPE, bg_type);
        contentValues.put(COL_BG_BALANCE, bg_balance);
        contentValues.put(COL_BG_ICON_ID, bg_icon_id);
        db.insert(TABLE_BG, null, contentValues);
    }

    public void insertCurrentBalance(int date, int balance){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_DATE, date);
        contentValues.put(COL_CURRENT_BALANCE, balance);
        db.insert(TABLE_CURRENT_BALANCE, null, contentValues);
    }

    public void insertMonthlyBudget(int currentMonth, int budget) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_MONTH, currentMonth);
        contentValues.put(COL_CURRENT_BUDGET, budget);
        db.insert(TABLE_CURRENT_BUDGET, null, contentValues);
    }

    public void updateCurAmount(int date, int curAmount) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_DATE, date);
        contentValues.put(COL_CURRENT_BALANCE, curAmount);
        db.update(TABLE_CURRENT_BALANCE, contentValues, COL_DATE + " = ?", new String[]{Integer.toString(date)});
        //Log.i("updateCurrentBalance", "Update Current Balance Date: " + date + " balance = " + curAmount);
    }

    public void updateUserProfile(String oldUsername, String username, int amountperday, int budget) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_USERNAME, username);
        contentValues.put(COL_AMOUNTPERDAY, amountperday);
        contentValues.put(COL_BUDGET, budget);
        db.update(TABLE_USER, contentValues, COL_USERNAME + " = ?", new String[]{oldUsername});
        //Log.i("updateCurrentBalance", "Update " + TABLE_USER + " username: " + username + " budget = " + budget);
    }

    public void updateCurrentBudget(int month, int budget) {
        //Log.d(TAG_NAME,"update budget month = " + month + " budget = " + budget);
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_CURRENT_BUDGET, budget);
        db.update(TABLE_CURRENT_BUDGET, contentValues, COL_MONTH + " = ?" , new String[]{Integer.toString(month)});
        Log.i("UpdateCurrentBudget", "Update" + TABLE_CURRENT_BUDGET + "month = " + month + " , budget = " + budget);
    }

    //UPDATE goal Money Saved
    public void updateGoalSaved(int goal_id, int goal_new_saved ){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_GOAL_ID, goal_id);
        contentValues.put(COL_GOAL_MSAVE,goal_new_saved);
        db.update(TABLE_GOAL, contentValues, COL_GOAL_ID + " = " + goal_id, null);
        //Log.i("UpdateGoalSaved", "Update" + TABLE_GOAL + " goal ID = " + goal_id + " new money saved = " + goal_new_saved);
    }

    //
    public void updateBudgetBalance (int budget_id, int budget_add_amount){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BG_ID, budget_id);
        contentValues.put(COL_BG_BALANCE, budget_add_amount);
        db.update(TABLE_BG, contentValues, COL_BG_ID + " = " + budget_id, null);
        //Log.i("UpdateBudgetAmount", "Update " + TABLE_BG + " BG_ID = " + budget_id + " incoming amount = " + budget_add_amount);
    }

    //DELETE daily budget
    public void deleteDailyBGItem(int id) {
        String whereClause =  COL_DAILY_LIST_ID + " = ?";
        String[] whereArgs = new String[]{Integer.toString(id)};
        db.delete(TABLE_DAILYBG, whereClause, whereArgs);
    }

    public void deleteGoal(int id) {
        String whereClause =  COL_GOAL_ID + " = ?";
        String[] whereArgs = new String[]{Integer.toString(id)};
        db.delete(TABLE_GOAL, whereClause, whereArgs);
    }

    public void deleteBudget(int id) {
        String whereClause =  COL_BG_ID + " = ?";
        String[] whereArgs = new String[]{Integer.toString(id)};
        db.delete(TABLE_BG, whereClause, whereArgs);
    }
}
