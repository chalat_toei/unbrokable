package th.ac.kku.coe.unbrokable.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.adapter.BudgetAdapter;
import th.ac.kku.coe.unbrokable.manager.DataManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetFragment extends Fragment {

    DataManager dm;

    static  BudgetFragment budgetFragment;

    private View view;
    private ListView listView;
    private TextView tvBudget;
    private ImageButton imgBtnAddTotalBudget;
    private BudgetAdapter adapter;
    private FrameLayout noListBG;


    public BudgetFragment() {
        // Required empty public constructor
        super();
    }

    public static BudgetFragment newInstance() {
        budgetFragment = new BudgetFragment();
        return budgetFragment;
    }

    public static BudgetFragment getBudgetFragment() {
        return budgetFragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        updateBudgetView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_budget, container, false);

        initInstances();

        adapter = new BudgetAdapter(getContext(), dm.getBudgetIconId(), dm.getBudgetBalance(), dm.getBudgetType(), BudgetFragment.this);
        listView.setAdapter(adapter);

        if(adapter.getCount() != 0) {
            noListBG.setVisibility(View.GONE);
        }

        imgBtnAddTotalBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .title("Add your budget amount")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input("How much do you want to add?", null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                try {
                                    //Log.i("Dialog", input.toString());
                                    int amount = Integer.parseInt(input.toString());
                                    dm.addUserBudget(amount);
                                    updateBudgetView();
                                } catch (NumberFormatException e) {
                                    Toast.makeText(getContext(), "please fill the amount that you want to add", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }).show();
            }
        });

        return view;
    }

    public void updateBudgetView() {
        dm.loadBudgetData();
        dm.loadProfileData(dm.getUsername());
        tvBudget.setText(dm.getCurrentBudgetLeft() + " ฿");
        adapter.setBudgetItemAdapter(dm.getBudgetIconId(), dm.getBudgetBalance(), dm.getBudgetType());
        adapter.notifyDataSetChanged();
        if(adapter.getCount() != 0) {
            noListBG.setVisibility(View.GONE);
        }
    }

    private void initInstances() {

        dm = DataManager.getInstance();

        listView = (ListView) view.findViewById(R.id.list_view_budget);
        tvBudget = (TextView) view.findViewById(R.id.tv_budget);
        imgBtnAddTotalBudget = (ImageButton) view.findViewById(R.id.btn_add_budget);
        noListBG = (FrameLayout) view.findViewById(R.id.bg_budget_begin);
    }


}
