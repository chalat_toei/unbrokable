package th.ac.kku.coe.unbrokable.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.fragment.DailyBudgetFragment;
import th.ac.kku.coe.unbrokable.manager.DataManager;

/**
 * Created by crazynova on 30/4/2559.
 */
public class DailyBudgetAdapter extends BaseAdapter {

    DataManager dm;

    Context mContext;

    DailyBudgetFragment dailyBudgetFragment;

    ArrayList<Integer> amountList;
    ArrayList<String> typeList, noteList;
    private TextView tv_amount,tv_note;
    private ImageView imgview_type;
    private ImageButton btn_delete;



    public DailyBudgetAdapter(Context context,ArrayList<Integer> amountList, ArrayList noteList, ArrayList typeList, DailyBudgetFragment dailyBudgetFragment) {
        this.mContext= context;
        this.amountList = amountList;
        this.noteList = noteList;
        this.typeList = typeList;
        this.dailyBudgetFragment = dailyBudgetFragment;
    }

    public void setDaliyBudgetItemAdapter(ArrayList<Integer> amountList, ArrayList noteList, ArrayList typeList) {
        this.amountList = amountList;
        this.noteList = noteList;
        this.typeList = typeList;
    }

    @Override
    public int getCount() {
        return amountList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View view, final ViewGroup parent) {
        LayoutInflater mInflater =
                (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        dm = DataManager.getInstance();

        //if(view == null) {
        view = mInflater.inflate(R.layout.custom_view_group_daily_budget_item, parent, false);
        view.setClickable(true);
        //}

        tv_amount = (TextView) view.findViewById(R.id.tv_amount);
        tv_note = (TextView) view.findViewById(R.id.tv_note);
        imgview_type = (ImageView) view.findViewById(R.id.img_view_type);
        btn_delete = (ImageButton) view.findViewById(R.id.img_btn_daily_budget_delete);

        tv_amount.setText(amountList.get(position) + " ฿");
        tv_note.setText(noteList.get(position));
        if(typeList.get(position).equals("income")) {
            imgview_type.setImageResource(R.drawable.add_money);
        } else if(typeList.get(position).equals("outcome")) {
            imgview_type.setImageResource(R.drawable.sub_money);
        }

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dm.loadDailyBudgetData();
                dm.deleteDailyItem(position);
                dailyBudgetFragment.updateDailyBudgetView();
            }
        });


        return view;
    }
}
