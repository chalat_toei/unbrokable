package th.ac.kku.coe.unbrokable.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.manager.DataManager;

public class AddBudgetActivity extends AppCompatActivity implements View.OnClickListener {

    DataManager dm;

    ImageView ivBGIcon;
    EditText etBGtype, etStartBGAmount;
    Button btnAddBudgetDone;
    int imgID = 0;
    ImageButton imgBtnEducation, imgBtnBook, imgBtnBus, imgBtnFood,
                imgBtnMedicare, imgBtnMovie, imgBtnPet, imgBtnShop,
                imgBtnSport, imgBtnTravel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budget);
        initInstance();
    }

    private void initInstance() {
        dm = DataManager.getInstance();

        ivBGIcon = (ImageView) findViewById(R.id.ivBGIcon);
        etBGtype = (EditText) findViewById(R.id.etBGtype);
        etStartBGAmount = (EditText) findViewById(R.id.etStartBGAmount);
        btnAddBudgetDone = (Button) findViewById(R.id.btnAddBudgetDone);

        imgBtnBook = (ImageButton) findViewById(R.id.icon_book);
        imgBtnBus = (ImageButton) findViewById(R.id.icon_bus);
        imgBtnEducation = (ImageButton) findViewById(R.id.icon_education);
        imgBtnFood = (ImageButton) findViewById(R.id.icon_food);
        imgBtnMedicare = (ImageButton) findViewById(R.id.icon_medicare);
        imgBtnMovie = (ImageButton) findViewById(R.id.icon_movie);
        imgBtnPet = (ImageButton) findViewById(R.id.icon_pet);
        imgBtnShop = (ImageButton) findViewById(R.id.icon_shop);
        imgBtnSport = (ImageButton) findViewById(R.id.icon_sport);
        imgBtnTravel = (ImageButton) findViewById(R.id.icon_travel);

        btnAddBudgetDone.setOnClickListener(this);
        imgBtnBook.setOnClickListener(this);
        imgBtnTravel.setOnClickListener(this);
        imgBtnSport.setOnClickListener(this);
        imgBtnBus.setOnClickListener(this);
        imgBtnShop.setOnClickListener(this);
        imgBtnEducation.setOnClickListener(this);
        imgBtnMedicare.setOnClickListener(this);
        imgBtnMovie.setOnClickListener(this);
        imgBtnPet.setOnClickListener(this);
        imgBtnFood.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        //Log.d("TAG", "click " + imgID);

        if(v == btnAddBudgetDone) {
            if (etBGtype.getText().toString().equals("")){
                Toast.makeText(AddBudgetActivity.this, R.string.toast_add_budget_type, Toast.LENGTH_SHORT).show();
            }else if (imgID == 0){
                Toast.makeText(AddBudgetActivity.this, R.string.toast_add_budget_selectIcon, Toast.LENGTH_SHORT).show();
            }else{
                try{
                    dm.addBudgetItem(etBGtype.getText().toString(), Integer.parseInt(etStartBGAmount.getText().toString()), imgID);
                    Intent intent = new Intent(AddBudgetActivity.this, MainActivity.class);
                    intent.putExtra("selectFragment", 1);
                    startActivity(intent);
                }catch (NumberFormatException e){
                    Toast.makeText(AddBudgetActivity.this, R.string.toast_add_budget_amount, Toast.LENGTH_SHORT).show();
                }
            }
        }
        if(v == imgBtnBook)
            imgID = 1;
        if(v == imgBtnBus)
            imgID = 2;
        if(v == imgBtnEducation)
            imgID = 3;
        if(v == imgBtnFood)
            imgID = 4;
        if(v == imgBtnMedicare)
            imgID = 5;
        if(v == imgBtnMovie)
            imgID = 6;
        if(v == imgBtnPet)
            imgID = 7;
        if(v == imgBtnShop)
            imgID = 8;
        if(v == imgBtnSport)
            imgID = 9;
        if(v == imgBtnTravel)
            imgID = 10;
        }

}
