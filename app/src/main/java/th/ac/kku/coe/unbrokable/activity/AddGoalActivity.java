package th.ac.kku.coe.unbrokable.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.manager.DataManager;

public class AddGoalActivity extends AppCompatActivity {
    private static final int RESULT_LOAD_IMAGE = 1;
    EditText etGoalName, etGoalPrice, etGoalSaved;
    Button btnAddGoalDone;
    FloatingActionButton fabSelectGoalImg;
    ImageView ivGoalImg;
    Toolbar toolbar;
    Uri selectedImage;
    DataManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_goal);
        initInstance();



        fabSelectGoalImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
            }
        });

        btnAddGoalDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int goalSaved;
                if (selectedImage == null) {
                    Toast.makeText(AddGoalActivity.this, R.string.toast_add_goal_selectPic, Toast.LENGTH_SHORT).show();
                } else if(etGoalName.getText().toString().equals("")){
                    Toast.makeText(AddGoalActivity.this, R.string.toast_add_goal_goal, Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        goalSaved = Integer.parseInt(etGoalSaved.getText().toString());
                    } catch (NumberFormatException e) {
                        goalSaved = 0;
                    }
                    try {
                        String goalName = etGoalName.getText().toString();
                        int goalPrice = Integer.parseInt(etGoalPrice.getText().toString());
                        dm.addGoalItem(goalName, goalPrice, goalSaved, selectedImage);
                        Intent intent = new Intent(AddGoalActivity.this, MainActivity.class);
                        intent.putExtra("selectFragment", 2);
                        startActivity(intent);
                    } catch (NumberFormatException e) {
                        Toast.makeText(AddGoalActivity.this, R.string.toast_add_goal_price, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void initInstance() {
        dm = DataManager.getInstance();

        etGoalName = (EditText) findViewById(R.id.etGoalName);
        etGoalPrice = (EditText) findViewById(R.id.etGoalPrice);
        etGoalSaved = (EditText) findViewById(R.id.etGoalSaved);
        fabSelectGoalImg = (FloatingActionButton) findViewById(R.id.fabSelectGoalImg);
        btnAddGoalDone = (Button) findViewById(R.id.btnAddGoalDone);
        ivGoalImg = (ImageView) findViewById(R.id.ivGoalImg);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && data != null){
            //Log.i("Checkimgloading", "load img ok");
            selectedImage = data.getData();
            if (selectedImage != null) {
                //Log.d("picture manage", "imgPath != null");
                Glide.with(AddGoalActivity.this)
                        .load(selectedImage)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .into(ivGoalImg);
            }
            //Log.i("URI", selectedImage.toString());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}



