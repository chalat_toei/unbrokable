package th.ac.kku.coe.unbrokable.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.manager.DataManager;
import th.ac.kku.coe.unbrokable.adapter.GoalItemAdapter;
import th.ac.kku.coe.unbrokable.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GoalFragment extends Fragment {

    DataManager dm;
    final static String TAG = "checkonfunc";

    FrameLayout noListGoal;
    private View view;
    ListView listGoal;

    ArrayList<String> nameList = new ArrayList<>(),imgURIList = new ArrayList<>();
    ArrayList<Integer> priceList = new ArrayList<>(),currentMSave = new ArrayList<>();

    GoalItemAdapter adapter;

    public GoalFragment() {

        super();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.d(TAG, "on Resume");
        updateListView();
    }

    public void updateListView() {
        dm.loadGoalData();
        adapter.setGoalItemAdapter(dm.getGoalNameList(), dm.getGoalPriceList(), dm.getGoalImageURIList(), dm.getGoalSavedList());
        adapter.notifyDataSetChanged();
        if(adapter.getCount() != 0) {
            noListGoal.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dm = DataManager.getInstance();
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_goal, container, false);
        listGoal = (ListView) view.findViewById(R.id.list_goal);
        noListGoal = (FrameLayout) view.findViewById(R.id.bg_goal_begin);

        //Log.d(TAG, "on CreateView");

        initInstances();

        adapter = new GoalItemAdapter(this.getContext(), dm.getGoalNameList(), dm.getGoalPriceList(), dm.getGoalImageURIList(), dm.getGoalSavedList(), GoalFragment.this);
        listGoal.setAdapter(adapter);

        if(adapter.getCount() != 0) {
            noListGoal.setVisibility(View.GONE);
        }
        return view;
    }

    private void initInstances() {
        nameList = dm.getGoalNameList();
        priceList = dm.getGoalPriceList();
        imgURIList = dm.getGoalImageURIList();
        currentMSave = dm.getGoalSavedList();
    }




}
