package th.ac.kku.coe.unbrokable.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.manager.DataManager;

public class ShowProfileActivity extends AppCompatActivity {

    DataManager dm;

    TextView tvUsername, tvBudgetPerDay, tvBudgetTotal;
    Button btnShowProfileEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_profile);
        initInstance();

        btnShowProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowProfileActivity.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });

    }

    private void initInstance() {
        dm = DataManager.getInstance();

        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvBudgetPerDay = (TextView) findViewById(R.id.tvBudgetPerDay);
        tvBudgetTotal = (TextView) findViewById(R.id.tvBudgetTotal);
        btnShowProfileEdit = (Button) findViewById(R.id.btnShowProfileEdit);

        String username = dm.getUsername();
        tvUsername.setText(username);
        tvBudgetPerDay.setText(dm.getAmountperday(username) + "");
        tvBudgetTotal.setText(dm.getCurrentBudget() + "");
    }


}
