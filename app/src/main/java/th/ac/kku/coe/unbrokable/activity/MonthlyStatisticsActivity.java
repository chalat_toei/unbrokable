package th.ac.kku.coe.unbrokable.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.manager.DataManager;

public class MonthlyStatisticsActivity extends AppCompatActivity {

    DataManager dm;
    YAxis leftAxis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_statistics);
        dm = DataManager.getInstance();
        initGraph();
    }

    private void initGraph() {

        BarChart chart = (BarChart) findViewById(R.id.chart2);
        XAxis xAxis = chart.getXAxis();
        leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();
        xAxis.setDrawGridLines(false);
        leftAxis.setDrawGridLines(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setEnabled(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(Float.parseFloat("10"));
        leftAxis.setAxisMinValue(0);
        leftAxis.setDrawZeroLine(true);
        leftAxis.setZeroLineColor(getResources().getColor(R.color.colorAccent));

        chart.setDescription("");

        //Data Manage
        ArrayList<BarEntry> valsComp = getvalsComp();
        ArrayList<String> xVals = getTextMonth(dm.getMonthList());

        BarDataSet setComp = new BarDataSet(valsComp, "Left amount");
        setComp.setBarSpacePercent(20);
        setComp.setAxisDependency(YAxis.AxisDependency.LEFT);
        setComp.setColor(getResources().getColor(R.color.cyan));
        setComp.setValueTextSize(10);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(setComp);

        BarData data = new BarData(xVals, dataSets);
        chart.setData(data);
        chart.notifyDataSetChanged();
        chart.invalidate();
        chart.setVisibleXRangeMaximum(4);
        chart.setVisibleXRangeMinimum(4);
        chart.animateY(1200);
        chart.moveViewToX(xVals.size() - 1);


    }



    @NonNull
    private ArrayList<BarEntry> getvalsComp() {
        ArrayList monthlyBudgetList = dm.getMonthlyBudgetList();
        ArrayList<BarEntry> valsComp = new ArrayList<>();

        for (int i = 0; i < monthlyBudgetList.size(); i++) {
            Float value = Float.parseFloat(monthlyBudgetList.get(i).toString());
            if(value < 0) {
                leftAxis.resetAxisMinValue();
            }
            BarEntry entry = new BarEntry(value, i);
            valsComp.add(entry);
        }
        return valsComp;
    }


    private ArrayList<String> getTextMonth(ArrayList<Integer> monthList) {
        ArrayList<String> monthTextList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < monthList.size(); i++) {
            String monthInt = monthList.get(i).toString();
            sb.append(selectMonth(monthInt.substring(4, 6)).concat(" "));
            sb.append(monthInt.substring(0, 4));
            monthTextList.add(sb.toString());
            sb.setLength(0);
        }
        return  monthTextList;
    }

    private String selectMonth(String monthInNumber) {
        String monthInStr = "";
        switch (monthInNumber) {
            case "01": monthInStr = "Jan";
                break;
            case "02": monthInStr = "Feb";
                break;
            case "03": monthInStr = "Mar";
                break;
            case "04": monthInStr = "Apr";
                break;
            case "05": monthInStr = "May";
                break;
            case "06": monthInStr = "Jun";
                break;
            case "07": monthInStr = "Jul";
                break;
            case "08": monthInStr = "Aug";
                break;
            case "09": monthInStr = "Sept";
                break;
            case "10": monthInStr = "Oct";
                break;
            case "11": monthInStr = "Nov";
                break;
            case "12": monthInStr = "Dec";
                break;
        }
        return monthInStr;
    }

}
