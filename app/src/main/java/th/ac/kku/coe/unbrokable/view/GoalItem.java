package th.ac.kku.coe.unbrokable.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import th.ac.kku.coe.unbrokable.R;

/**
 * Created by crazynova on 25/4/2559.
 */
public class GoalItem extends FrameLayout {

    ImageView img;
    RoundCornerProgressBar probar;
    TextView  tvName, tvPrice;

    public GoalItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public GoalItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
    }

    public GoalItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
    }
    @TargetApi(21)
    public GoalItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
    }

    private void initInflate() {
        inflate(getContext(), R.layout.custom_view_group_goal_item, this);
    }

    private void initInstances() {
        img = (ImageView) findViewById(R.id.goal_item_img);
        tvName = (TextView) findViewById(R.id.goal_item_name);
        tvPrice = (TextView) findViewById(R.id.goal_item_price);
        probar = (RoundCornerProgressBar) findViewById(R.id.goal_item_probar);
    }

    /*
    public void setImg(int pic_id) {
        img.setImageResource(pic_id);
    }

    public void setName(String name) {
        tvName.setText(name);
    }

    public void setPrice(int price) {
        tvPrice.setText(price + " ฿");
    }

    public void setProbar(int currentMSave) {
        probar.setBackgroundResource(currentMSave);
    }

    public void initGoalItem(int pic_id, String name, int price, int currentMSave) {
        setImg(pic_id);
        setName(name);
        setPrice(price);
        setProbar(currentMSave);
    }
    */
}
