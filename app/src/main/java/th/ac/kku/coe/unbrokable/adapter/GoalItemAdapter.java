package th.ac.kku.coe.unbrokable.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.activity.MainActivity;
import th.ac.kku.coe.unbrokable.fragment.BudgetFragment;
import th.ac.kku.coe.unbrokable.fragment.GoalFragment;
import th.ac.kku.coe.unbrokable.manager.DataManager;

/**
 * Created by crazynova on 1/5/2559.
 */
public class GoalItemAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> name, imgURI;
    ArrayList<Integer> price, currentMSave;
    ImageView img;
    RoundCornerProgressBar probar;
    TextView tvName, tvPrice, tvProgress;
    Button btnAdd,btnRemove;
    DataManager dm;
    GoalFragment goalFragment;

    public GoalItemAdapter (Context context, ArrayList name, ArrayList price, ArrayList imgId, ArrayList currentMSave, GoalFragment goalFragment) {
        this.context = context;
        this.name = name;
        this.price = price;
        this.imgURI = imgId;
        this.currentMSave = currentMSave;
        this.goalFragment = goalFragment;
    }

    public void setGoalItemAdapter(ArrayList name, ArrayList price, ArrayList imgId, ArrayList currentMSave) {
        this.name = name;
        this.price = price;
        this.imgURI = imgId;
        this.currentMSave = currentMSave;
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, final ViewGroup parent) {
        LayoutInflater mInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = mInflater.inflate(R.layout.custom_view_group_goal_item, parent, false);
        dm = DataManager.getInstance();

        tvName = (TextView) view.findViewById(R.id.goal_item_name);
        tvPrice = (TextView) view.findViewById(R.id.goal_item_price);
        img = (ImageView) view.findViewById(R.id.goal_item_img);
        probar = (RoundCornerProgressBar) view.findViewById(R.id.goal_item_probar);
        btnAdd = (Button) view.findViewById(R.id.btn_goal_add);
        btnRemove = (Button) view.findViewById(R.id.btn_goal_remove);
        tvProgress = (TextView) view.findViewById(R.id.tv_progress);

        tvName.setText(name.get(position));
        tvPrice.setText(price.get(position) + " ฿");
       // File picture = new File(imgURI.get(position).toString());
        /*
        try {
             img.setImageBitmap(scaleCenterCrop(MediaStore.Images.Media.getBitmap(goalFragment.getContext().getContentResolver(), Uri.parse(imgURI.get(position).toString())), 200, 200));

        } catch (IOException e) {
             e.printStackTrace();
        }
        */
        Glide.with(context)
                .load(Uri.parse(imgURI.get(position)))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .override(200, 200)
                .into(img);

        probar.setMax(price.get(position));
        probar.setProgress(currentMSave.get(position));
        tvProgress.setText(currentMSave.get(position) + "");


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(context)
                        .title(R.string.txt_dialog_saved_title)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(context.getString(R.string.txt_dialog_saved_hint), null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                try {
                                    //Log.i("Dialog", input.toString());
                                    int amount = Integer.parseInt(input.toString());
                                    dm.addGoalMSaved(context, position, amount);
                                    goalFragment.updateListView();
                                } catch (NumberFormatException e) {
                                    Toast.makeText(context, R.string.txt_dialog_saved_toast, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).show();
                //Log.i("checkclick", "click add : " + position);
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(context)
                        .title(R.string.txt_dialog_remove_title)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(context.getString(R.string.txt_dialog_remove_hint), null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                try {
                                    //Log.i("Dialog", input.toString());
                                    int amount = Integer.parseInt(input.toString());
                                    dm.removeGoalMSaved(position, amount);
                                    goalFragment.updateListView();
                                } catch (NumberFormatException e) {
                                    Toast.makeText(context, R.string.txt_dialog_remove_toast, Toast.LENGTH_SHORT).show();
                                }

                            }
                        }).show();
                //Log.i("checkclick", "click remove : " + position);
            }
        });

        return view;
    }

}
