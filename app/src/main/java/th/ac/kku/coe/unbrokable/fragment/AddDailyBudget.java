package th.ac.kku.coe.unbrokable.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import th.ac.kku.coe.unbrokable.R;
import th.ac.kku.coe.unbrokable.activity.AddDailyBudgetActivity;
import th.ac.kku.coe.unbrokable.activity.MainActivity;
import th.ac.kku.coe.unbrokable.manager.DataManager;


/**
 * Created by crazynova.
 */
public class AddDailyBudget extends Fragment {

    private SimpleDateFormat mFormatterDate = new SimpleDateFormat("dd MMMM yyyy");
    private SimpleDateFormat mFormattertime = new SimpleDateFormat("hh:mm aa");
    private SimpleDateFormat mFormatterDateInt = new SimpleDateFormat("yyyyMMdd");
    TextView tvDatePK,tvTime;
    EditText edtAmount, edtNote;
    RadioGroup oparater;
    Button done,btnEditDate;
    DataManager dm;

    String type, note, date, time;
    int amount, dateInt;
    String toast_add_daily_budget_amount;

    public AddDailyBudget() {
        super();
    }

    public static AddDailyBudget newInstance() {
        AddDailyBudget fragment = new AddDailyBudget();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frament_add_daily_budget, container, false);
        dm = DataManager.getInstance();
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        tvDatePK = (TextView) rootView.findViewById(R.id.datePicker);
        tvTime = (TextView) rootView.findViewById(R.id.timePicker);
        edtAmount = (EditText)rootView.findViewById(R.id.amount);
        edtNote = (EditText)rootView.findViewById(R.id.note);
        oparater = (RadioGroup)rootView.findViewById(R.id.rgOparater);
        done = (Button)rootView.findViewById(R.id.done);
        btnEditDate = (Button) rootView.findViewById(R.id.btn_daily_edit_date);

        toast_add_daily_budget_amount = getResources().getString(R.string.toast_add_daily_budget_amount);

        dm.setTime(mFormattertime.format(Calendar.getInstance().getTime()));
        dm.setDate(mFormatterDate.format(Calendar.getInstance().getTime()));

        tvDatePK.setText(dm.getDate());
        tvTime.setText(dm.getTime());

        edtAmount.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtAmount, InputMethodManager.SHOW_IMPLICIT);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    amount = Integer.parseInt(edtAmount.getText().toString());
                    note = edtNote.getText().toString();
                    time = dm.getTime();
                    date = dm.getDate();
                    type = "";
                    checkIncomeOrExpense();
                    dm.setAmount(amount);
                    dm.setNote(note);
                    dm.setType(type);
                    dm.addDailyBudgetItem(amount, type, date, time, note, dateInt);
                    //Log.i("Transition", "Start MainActivity");
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    startActivity(intent);
                } catch (NumberFormatException e) {
                    Toast.makeText(getContext(), toast_add_daily_budget_amount, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnEditDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                                //.setMinDate(minDate)
                                //.setMaxDate(maxDate)
                                //.setIs24HourTime(true)
                                //.setTheme(SlideDateTimePicker.HOLO_DARK)
                                //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();

            }
        });

    }

    private void checkIncomeOrExpense() {
        switch (oparater.getCheckedRadioButtonId()) {
            case R.id.rbAdd:
                type = "income";

                break;
            case R.id.rbSub:
                type = "outcome";
                break;}
    }

    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            dm.setDate(mFormatterDate.format(date));
            dm.setTime(mFormattertime.format(date));

            tvDatePK.setText(dm.getDate());
            tvTime.setText(dm.getTime());

            dateInt = Integer.parseInt(mFormatterDateInt.format(date));
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel()
        {
            Toast.makeText(getContext(),"Canceled", Toast.LENGTH_SHORT).show();
        }


    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

}
