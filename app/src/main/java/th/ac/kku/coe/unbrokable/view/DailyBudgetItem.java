package th.ac.kku.coe.unbrokable.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import th.ac.kku.coe.unbrokable.R;

/**
 * Created by crazynova on 24/4/2559.
 */
public class DailyBudgetItem extends FrameLayout {

    private TextView tv_amount;

    public DailyBudgetItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public DailyBudgetItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
    }

    public DailyBudgetItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
    }
    @TargetApi(21)
    public DailyBudgetItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
    }

    private void initInflate() {
        inflate(getContext(), R.layout.custom_view_group_daily_budget_item, this);
    }

    private void initInstances() {
        tv_amount = (TextView) findViewById(R.id.tv_amount);
    }

}
