package th.ac.kku.coe.unbrokable.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by crazynova.
 */
public class DataManager {

    public final String TAG = "DatamanagerTAG";

    private static int DEFAULT_AMOUNT_PER_DAY = 150;
    private static int DEFAULT_BUDGET = 10000;

    private SimpleDateFormat mFormatterDate = new SimpleDateFormat("dd MMMM yyyy");
    private SimpleDateFormat mFormatterDateInt = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat mFormatterMonth = new SimpleDateFormat("yyyyMM");

    private static DataManager instance;
    private DBHelper dbHelper;

    //User Profile data
    ArrayList<Integer> userProfileList;
    //Daily Budget data
    private ArrayList<Integer> amountList, amountidList;
    private ArrayList<String> typeList,dateList,timeList,noteList;
    //Goal data
    private ArrayList<Integer> goalIdList = new ArrayList<>();
    private ArrayList<String> goalNameList = new ArrayList<>();
    private ArrayList<Integer> goalPriceList = new ArrayList<>();
    private ArrayList<Integer> goalSavedList = new ArrayList<>();
    private ArrayList<String> goalImageURIList = new ArrayList<>();
    //Budget
    private  ArrayList<Integer> budgetIdList = new ArrayList<>();
    private  ArrayList<Integer> budgetIconIdList = new ArrayList<>();
    private  ArrayList<String> budgetBalanceList = new ArrayList<>();
    private  ArrayList<String> budgetTypeList = new ArrayList<>();
    //temp
    private int amount,currentMonth;
    private String type,note,time,date;

    public String username;
    private String currentDate;
    public int amountperday,currentAmount,budgetpermonth;

    public static DataManager getInstance() {
        if (instance == null)
            instance = new DataManager();
        return instance;
    }

    private Context mContext;

    private DataManager() {
        mContext = Contextor.getInstance().getContext();
        dbHelper = new DBHelper(mContext);
        amountList = new ArrayList<>();
        typeList = new ArrayList<>();
        dateList = new ArrayList<>();
        timeList = new ArrayList<>();
        noteList = new ArrayList<>();
        userProfileList = new ArrayList<>();
        //initInstance
        currentDate = getCurrentDate();
        currentMonth = getCurrentMonth();
        amountperday = getAmountperday(getUsername());
        currentAmount = getCurrentAmount(currentDate);

        loadProfileData(getUsername());
        loadDailyBudgetData();
        loadGoalData();
        loadBudgetData();
    }


    //Load

    public void loadProfileData(String username) {
         this.username = username;
         ArrayList<Integer> userDocList = dbHelper.selectByUsername(username);
         this.amountperday = userDocList.get(0);
         this.budgetpermonth = userDocList.get(1);
    }

    public void loadDailyBudgetData() {
        //Log.i("Transition", "Load daily budget from database");
        currentDate = getCurrentDate();
        dbHelper.scanTableDailyBudget(currentDate);
        amountidList = dbHelper.getamountIdList();
        amountList = dbHelper.getamountList();
        typeList = dbHelper.gettypeList();
        dateList = dbHelper.getdateList();
        timeList = dbHelper.gettimeList();
        noteList = dbHelper.getnoteList();
    }

    public void loadGoalData() {
        goalIdList = dbHelper.selectGoalID();
        goalNameList = dbHelper.selectGoalName();
        goalPriceList = dbHelper.selectGoalPrice();
        goalSavedList = dbHelper.selectGoalMSaved();
        goalImageURIList = dbHelper.selectGoalImgURI();
    }

    public void loadBudgetData() {
        budgetIdList = dbHelper.selectBGId();
        budgetIconIdList = dbHelper.selectBGIconID();
        budgetBalanceList = dbHelper.selectBGBalance();
        budgetTypeList = dbHelper.selectBGType();
    }

    //add
    public void addDailyBudgetItem(int amount, String type, String date, String time, String note, int dateInt) {
        String today = mFormatterDate.format(new Date().getTime());
        int todayInt = Integer.parseInt(mFormatterDateInt.format(new Date().getTime()));
        if(date.equals(today)) {
            currentAmount = getCurrentAmount(getUsername());
            if(type.equals("income")) {
                currentAmount = currentAmount + amount;
                dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() + amount);
            } else if(type.equals("outcome")) {
                currentAmount = currentAmount - amount;
                dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() - amount);
            }
            dbHelper.updateCurAmount(todayInt, currentAmount);
        } else {
            int tempAmount = getAmountByDate(getUsername(), dateInt);
            int month = convertDateToMonth(dateInt);
            if (type.equals("income")) {
                tempAmount = tempAmount + amount;
                dbHelper.updateCurrentBudget(month, getBudgetByMonth(month) + amount);
            } else if(type.equals("outcome")) {
                int oldAmount = getBudgetByMonth(month);
                tempAmount = tempAmount - amount;
                dbHelper.updateCurrentBudget(month, oldAmount - amount);
            }
            dbHelper.updateCurAmount(dateInt, tempAmount);
        }
        dbHelper.insertDailyBGItem(amount, type, date, time, note);
    }

    private int getBudgetByMonth(int month) {
        return dbHelper.selectBudgetByMonth(month);
    }



    private int convertDateToMonth(int dateInt) {
        String dateText = Integer.toString(dateInt);
        dateText = dateText.substring(0,6);
        return Integer.parseInt(dateText);
    }

    public void addGoalItem(String goalName, int goalPrice, int goalSaved, Uri selectedImage) {
        // data base insert data here
        String uri = selectedImage.toString();
        dbHelper.insertGoalItem(goalName, goalPrice, goalSaved, uri);
    }

    public void addBudgetItem(String bg_type, int bg_balance, int bg_icon_id) {
        int oldBudget = getCurrentBudget();
        int newBudget = oldBudget - bg_balance;
        dbHelper.updateCurrentBudget(getCurrentMonth(), newBudget);
        dbHelper.insertBudgetItem(bg_type, bg_balance, bg_icon_id);
    }



    //update
    public void updateUserProfileData(String username, int amountperday, int budget) {
        int oldAmountperday = getAmountperday(this.getUsername());
        int oldCurrentAmount = this.getCurrentAmount(this.getUsername());
        int newCurrentAmount = amountperday + (oldCurrentAmount - oldAmountperday);
        dbHelper.updateCurAmount(getCurrentDateInt(), newCurrentAmount);
        dbHelper.updateUserProfile(this.getUsername(), username, amountperday, budget);
        loadProfileData(username);
    }

    public void addGoalMSaved(Context context, int position, int amount) {
        int goalPrice = getGoalPriceList().get(position);
        int id = getGoalIdList().get(position);
        int oldMSaved = dbHelper.selectMSavedByID(id);
        int newMSaved = oldMSaved + amount;
        if(newMSaved >= goalPrice) {
            Toast.makeText(context, "Congratulations! You've already achieved your " + getGoalNameList().get(position) + " goal",Toast.LENGTH_SHORT).show();
            dbHelper.deleteGoal(id);
        } else {
            dbHelper.updateGoalSaved(id, newMSaved);
        }
        dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() - amount);
    }

    public void removeGoalMSaved(int position, int amount) {
        int id = getGoalIdList().get(position);
        int oldMSaved = dbHelper.selectMSavedByID(id);
        dbHelper.updateGoalSaved(id, oldMSaved - amount);

        dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() + amount);
    }

    public void addBudgetBalance(int position, int amount) {
        int id = getBudgetIdList().get(position);
        int oldBalance = dbHelper.selectBalanceByID(id);
        dbHelper.updateBudgetBalance(id, oldBalance + amount);
        dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() - amount);
    }

    public void removeBudgetBalance(Context context, int position, int amount) {
        int id = getBudgetIdList().get(position);
        int oldBalance = dbHelper.selectBalanceByID(id);
        int newBalance = oldBalance - amount;
        if(newBalance <= 0) {
            Toast.makeText(context, "You've already spend all of you " + getBudgetType().get(position) + " budget", Toast.LENGTH_SHORT).show();
            dbHelper.deleteBudget(id);
            dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() + newBalance);
        } else {
            dbHelper.updateBudgetBalance(id, oldBalance - amount);
        }
    }


    //delete daily budget
    public void deleteDailyItem(int position) {
        int id = getAmountidList().get(position);
        String type = getTypeList().get(position);
        int amount = getAmountList().get(position);
        int today = getCurrentDateInt();
        int currentAmount = getCurrentAmount(username);
        if(type.equals("income")) {
            dbHelper.updateCurAmount(today, currentAmount - amount);
        } else if(type.equals("outcome")) {
            dbHelper.updateCurAmount(today, currentAmount + amount);
        }
        dbHelper.deleteDailyBGItem(id);
    }

    /*
    Getter
     */

    private String getCurrentDate() {
        Date date = new Date();
        date.getTime();
        return mFormatterDate.format(date);
    }

    private int getCurrentDateInt() {
        Date date = new Date();
        date.getTime();
        return Integer.parseInt(mFormatterDateInt.format(date));
    }

    private int getCurrentMonth() {
        Date date = new Date();
        date.getTime();
        return Integer.parseInt(mFormatterMonth.format(date));
    }

    //User Profile
    public String getUsername() {
        this.username = dbHelper.getDBUsername();
        if(username.equals("")) {
            username = "Guest";
            dbHelper.insertUserProfile(username, DEFAULT_AMOUNT_PER_DAY, DEFAULT_BUDGET);
        }
        return username;
    }

    public int getAmountperday(String username) {
        loadProfileData(username);
        return this.amountperday;
    }

    public void addUserBudget(int amount) {
        int budget = getCurrentBudget();
        budget += amount;
        //Log.d(TAG, "budget = " + budget);
        dbHelper.updateCurrentBudget(getCurrentMonth(), budget);
    }

    //Daily budget
    public int getAmountByDate(String username, int date) {
        int tempAmount = dbHelper.selectAmountByDate(date);
        if(tempAmount == -9999) {
            tempAmount = amountperday;
            dbHelper.insertCurrentBalance(date, tempAmount);
        }
        return tempAmount;
    }

    public int getCurrentAmount(String username) {
        currentAmount = dbHelper.selectAmountByDate(getCurrentDateInt());
        if(currentAmount == -9999) {
            int leftAmount = getLastDayAmount();
            int yesterday = getYesterday();
            if(getCurrentDateInt() >= yesterday) {
                dbHelper.updateCurrentBudget(getCurrentMonth(), getCurrentBudget() + leftAmount);
            }
            //Log.d(TAG,"update current Budget = " + getLastMonthBudgetLeft() + " + leftAmount = " + leftAmount);
            currentAmount = amountperday;
            dbHelper.insertCurrentBalance(getCurrentDateInt(), currentAmount);
        }
        return currentAmount;
    }

    private int getYesterday() {
        int yesterday = 0;
        int index = getDateList().size() - 1;
        if(index != -1) {
            yesterday =  getDateList().get(index);
        }
        return yesterday;
    }

    private int getLastDayAmount() {
        int leftAmount = 0;
        int index = getCurrentAmountList().size() - 1;
        if(index != -1) {
            leftAmount = (Integer) getCurrentAmountList().get(index);
        }
        return leftAmount;
    }

    public int getCurrentBudget() {
        int currentBudget = dbHelper.selectBudgetByMonth(getCurrentMonth());
        if(currentBudget == -9999) {
            int leftBudget = getLastMonthBudgetLeft();
            int lastMonth = getLastMonth();
            if(getCurrentMonth() > lastMonth) {
                currentBudget = getBudgetPerMonth(getUsername()) + leftBudget;
            } else {
                currentBudget = getBudgetPerMonth(getUsername());
            }
            dbHelper.insertMonthlyBudget(getCurrentMonth(), currentBudget);
            Log.d(TAG, "change month to " + getCurrentMonth() + " last month is " + lastMonth);
        }
        return currentBudget;
    }

    private int getLastMonthBudgetLeft() {
        int leftBudget = 0;
        int index = getMonthlyBudgetList().size() - 1;
        if(index != -1) {
            leftBudget = (Integer) getMonthlyBudgetList().get(index);
        }
        return leftBudget;
    }

    private int getLastMonth() {
        int lastMonth = 0;
        int index = getMonthList().size() - 1;
        if(index != -1) {
            lastMonth = getMonthList().get(index);
        }
        return lastMonth;
    }

    public ArrayList getCurrentAmountList() {
        return dbHelper.selectCurrentAmountList();
    }

    public ArrayList getMonthlyBudgetList() {
        return dbHelper.selectMonthlyBudgetList();
    }

    public  ArrayList<Integer> getDateList() {
        return dbHelper.selectDateList();
    }

    public ArrayList<Integer> getMonthList(){
        return dbHelper.selectMonthList();
    }

    public int getBudgetPerMonth(String username) {
        loadProfileData(username);
        return budgetpermonth;
    }

    public int getCurrentBudgetLeft (){
        int maxDate = 0;
        int curBGLeft;
        int curBG = getCurrentBudget();
        Integer curDate= getCurrentDateInt();
        String curDateStr = curDate.toString();
        String m = curDateStr.substring(4, 6);
        int y = Integer.parseInt(curDateStr.substring(0,4));
        int d = Integer.parseInt(curDateStr.substring(6,8));
       if(m.equals("01") || m.equals("03") || m.equals("05") || m.equals("07") || m.equals("08") || m.equals("10") || m.equals("12")){
           maxDate = 31;
       }else if (m.equals("04")|| m.equals("06") || m.equals("09") || m.equals("11")) {
           maxDate = 30;
       }else if (m.equals("02")){
           if( isLeapYear(y)){
               maxDate = 29;
           }else maxDate = 28;
       }
        curBGLeft = curBG -  ((maxDate-d) * getAmountperday(getUsername()));

        return curBGLeft;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<Integer> getAmountidList(){
        return  amountidList;
    }

    public ArrayList<Integer> getAmountList() {
        amountList = dbHelper.getamountList();
        return amountList;
    }

    public ArrayList<String> getTypeList() {
        typeList = dbHelper.gettypeList();
        return typeList;
    }

    public ArrayList<String> getNoteList() {
        noteList = dbHelper.getnoteList();
        return noteList;
    }

    public String getTime() {
        return time;
    }

    //Goal
    public ArrayList<Integer> getGoalIdList() {
        return goalIdList;
    }

    public ArrayList<String> getGoalNameList() {
        return goalNameList;
    }

    public ArrayList<Integer> getGoalPriceList() {
        return goalPriceList;
    }

    public ArrayList<Integer> getGoalSavedList() {
        return goalSavedList;
    }

    public ArrayList<String> getGoalImageURIList() {
        return goalImageURIList;
    }

    public  int getGoalMSaved(int position) {
        int id = getGoalIdList().get(position);
        return dbHelper.selectMSavedByID(id);
    }

    //Budget
    public ArrayList<Integer> getBudgetIdList() {
        return budgetIdList;
    }

    public ArrayList getBudgetType() {
        return  budgetTypeList;
    }

    public ArrayList getBudgetBalance() {
        return budgetBalanceList;
    }

    public ArrayList getBudgetIconId() {
        return budgetIconIdList;
    }


    /*
    Setter
     */
    //Daily Budget
    public void setAmount(int amount) {
        this.amount = amount;
    }


    public void setType(String type) {
        this.type = type;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCurrentBudget(int budget) {
        dbHelper.updateCurrentBudget(getCurrentMonth(), budget);
    }

    private boolean isLeapYear(int year) {
        boolean isLeapYear = false;
        if(year % 4 == 0) {
            if(year % 100 == 0) {
                if(year % 400 == 0) {
                    isLeapYear = true;
                } else {
                    isLeapYear = false;
                }
            } else {
                isLeapYear = true;
            }
        } else {
            isLeapYear = false;
        }
        return isLeapYear;
    }
}
