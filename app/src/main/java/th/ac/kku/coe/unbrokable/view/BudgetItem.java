package th.ac.kku.coe.unbrokable.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import th.ac.kku.coe.unbrokable.R;

/**
 * Created by crazynova on 3/5/2559.
 */
public class BudgetItem extends FrameLayout {

    ImageView budgetImage;
    TextView tvBudgetType, tvBudgetAmount;
    Button btnAdd, btnSpend;

    public BudgetItem(Context context) {
        super(context);
        initInflater();
        initInstances();
    }

    public BudgetItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflater();
        initInstances();
    }

    public BudgetItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflater();
        initInstances();
    }
    @TargetApi(21)
    public BudgetItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflater();
        initInstances();
    }

    private void initInflater() {
        inflate(getContext(), R.layout.custom_view_group_budget, this);
    }

    private void initInstances() {
        budgetImage = (ImageView) findViewById(R.id.img_view_budget);
        tvBudgetType = (TextView) findViewById(R.id.tv_budget_type);
        tvBudgetAmount = (TextView) findViewById(R.id.tv_budget_type);
        btnAdd = (Button) findViewById(R.id.btn_budget_add);
        btnSpend = (Button) findViewById(R.id.btn_budget_spend);
    }

    private void initBudgetItem(int imgId, String budgetType, int amount) {
        budgetImage.setImageResource(imgId);
        tvBudgetType.setText(budgetType);
        tvBudgetAmount.setText(amount + " ฿");
    }
}
